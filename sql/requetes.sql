-- Les noms recommandes des prot des entrees qui comportent le terme cardiac dans le champ commentaire
-- retour : accession et nom de la prot
SELECT prot_name_2_prot.accession, protein_names.prot_name
    FROM comments,protein_names,prot_name_2_prot
    -- on fait le lien entre nom, entree et commentaire
    WHERE prot_name_2_prot.accession = comments.accession
    -- on fait le lien entre l'identifiant du nom
    AND prot_name_2_prot.prot_name_id = protein_names.prot_name_id
    -- on regarde si le commentaire contient cardiac
    AND comments.txt_c LIKE '%cardiac%'
    -- on regarde si le nom est recommandé
    AND protein_names.name_kind = 'recommendedName'
;

-- Les noms recommandés des prot des entrees qui comporte le mot cle "long qt syndrome" dans un champ mot clé
-- retour : nom des proteines
SELECT protein_names.prot_name
    FROM keywords,entries_2_keywords,protein_names,prot_name_2_prot
    -- on verifie si le mot clé contient long qt syndrome
    WHERE UPPER(keywords.kw_label) LIKE '%LONG QT SYNDROME%'
    -- on verifie que le nom est recommandé
    AND protein_names.name_kind = 'recommendedName' 
    -- on fait le lien entre nom et mot cle
    AND entries_2_keywords.kw_id = keywords.kw_id
    AND prot_name_2_prot.accession = entries_2_keywords.accession
    AND prot_name_2_prot.prot_name_id = protein_names.prot_name_id
;

-- les entrees dont la sequence est la plus longue
-- retour : les accession
SELECT entries.accession 
    FROM entries,proteins
    -- on associe les entrees avec la ligne dans la table proteins correspondant
    WHERE entries.accession = proteins.accession
    -- si la longueur est maximale, on prend la ligne
    AND proteins.seqLength = (
        SELECT MAX(proteins.seqLength)
            FROM proteins
    )
;

-- les entrees ayant strict plus que 2 nom de gene
-- retour : accession, le nombre de gene
SELECT accession, COUNT ( gene_name_id )
    FROM entry_2_gene_name
    GROUP BY accession
    HAVING COUNT( gene_name_id) > 2
;

-- les entrees ayant le mot "channel" dans le nom des proteines
-- retour : accession, nom, sorte
SELECT prot_name_2_prot.accession,protein_names.prot_name,protein_names.name_kind
    FROM prot_name_2_prot,protein_names
    -- on fait le lien entre accession et nom de prot
    WHERE prot_name_2_prot.prot_name_id = protein_names.prot_name_id
    -- on regarde si le nom contient channel
    AND UPPER(protein_names.prot_name) LIKE '%CHANNEL%'
;

-- les entrees et le mot recommande associé qui sont associé aux mot clé contenant
-- long qt syndrome et short qt syndrome
SELECT ek1.accession, protein_names.prot_name
    FROM protein_names,prot_name_2_prot, keywords k2, keywords k1, entries_2_keywords ek1, entries_2_keywords ek2
    -- on fait le lien entre accession et keyword
    WHERE k1.kw_id = ek1.kw_id
    AND k2.kw_id = ek2.kw_id
    -- K1 est lorsque long qt syndrome
    AND UPPER(k1.kw_label) LIKE '%LONG QT SYNDROME%'
    -- K2 est lorsque short qt syndrome
    AND UPPER(k2.kw_label) LIKE '%SHORT QT SYNDROME%'
    -- l'intersection des deux sous ensembles
    AND ek1.accession = ek2.accession
    -- on fait le lien entre nom, entree et mot cle
    AND ek1.accession = prot_name_2_prot.accession
    AND prot_name_2_prot.prot_name_id = protein_names.prot_name_id
    -- on ne retient que le nom recommendé et complet
    AND protein_names.name_kind = 'recommendedName'
    AND protein_names.name_type = 'fullName'
;


SELECT dbref.db_ref, COUNT(dbref.accession)
    FROM dbref,entries_2_keywords,keywords
    -- on ne retient que les references de type GO
    WHERE dbref.db_type = 'GO'
    -- on fait le lien entre db ref, accession et mot cle
    AND dbref.accession = entries_2_keywords.accession
    AND entries_2_keywords.kw_id = keywords.kw_id
    -- on ne retient que les mot clé contenant le mot long qt syndrome
    AND UPPER(keywords.kw_label) LIKE '%LONG QT SYNDROME%'
    -- on regroute par db ref
    GROUP BY dbref.db_ref
    HAVING COUNT(dbref.accession) > 1
;

-- SELECT DISTINCT prot_name_2_prot.accession,p2.prot_name
--     FROM protein_names p1, protein_names p2, prot_name_2_prot,gene_names,entry_2_gene_name,comments
--     WHERE prot_name_2_prot.accession = entry_2_gene_name.accession
--     AND entry_2_gene_name.accession = comments.accession
--     AND p1.prot_name_id = prot_name_2_prot.prot_name_id
--     AND gene_names.gene_name_id = entry_2_gene_name.gene_name_id
--     AND UPPER(gene_names.gene_name) LIKE UPPER('%' || '' || '%')
--     AND UPPER(p1.prot_name) LIKE UPPER('%' || 'channel' || '%')
--     AND UPPER(comments.txt_c) LIKE UPPER('%' || '' || '%')
--     AND p2.prot_name_id = prot_name_2_prot.prot_name_id
--     AND p2.name_type = 'fullName' 
--     AND p2.name_kind = 'recommendedName'
-- ;