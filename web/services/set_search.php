<?php
// Ce script attend 3 arguments. Il renvoie un ensemble d'accession avec le nom recommandé si il existe
require_once __DIR__."/../lib/services_commons.php";

$gene = $_REQUEST['gene_name'];
$prot = $_REQUEST['prot_name'];
$comment = $_REQUEST['comment'];

$res = $data->set_search($gene,$prot,$comment);
$data->close();

if (is_null($res)) {
    produceError("Une erreur est survenue!");
} else {
    produceResult($res);
}