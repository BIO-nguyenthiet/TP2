<?php
// Ce script attend 3 arguments. Il renvoie un ensemble d'accession avec le nom recommandé si il existe
require_once __DIR__."/../lib/services_commons.php";

$accession = $_REQUEST['accession'];

$res = $data->get_entry($accession);

// inutile de faire d'autre requete si l'accession n'existe pas
if (count($res) > 0) {
    $res[] = $data->get_prot_names($accession);
    $res[] = $data->get_gene_names($accession);
    $res[] = $data->get_keywords($accession);
    $res[] = $data->get_comments($accession);
    $res[] = $data->get_dbref($accession);
}

$data->close();
if (is_null($res)) {
    produceResult("Une erreur est survenue!");
} else {
    produceResult($res);
}