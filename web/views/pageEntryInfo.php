<!DOCTYPE html>
<html>
<?php
// Ce script présente un formulaire de recherche d'accession
// et permet d'en afficher les informations correspondante
?>
<head>
    <meta charset="UTF-8">
    <title>Entry Info</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/html.js"></script>
    <script src="js/single_search.js"></script>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/single_search.css">
</head>
<body dataset-accession="<?php echo $accession?>">

    <h1>Vous êtes en train de rechercher par identifiant</h1>
    <a href="index.php">Rechercher une entrée par chaîne ➜</a>
    <form id="single_search" action="services/single_search.php" >
         <input type="text" id="form_accession" name="accession" value="<?php echo $accession?>">
         <button type="submit">Info Entry</button>
    </form>

    <div id="single_search_result">
    </div>
</body>

</html>
