<?php
// Ce script peut attendre un accession en argument, il charge la page correspondante 
// avec en mémoire l'accession
$accession = null;
if (isset($_REQUEST['accession'])) {
    $accession = $_REQUEST['accession'];
}
require_once __DIR__ . "/views/pageEntryInfo.php";