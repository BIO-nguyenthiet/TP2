// script faisant du formattage html

function makeTable(t, id, dimensions, option) {
    // si pas d'option, on mets les options par défaut
    if (option == null) {
        option = {
            'link': '',
            'link_fields': ['']
        }
    }

    let table = create('table', id, null, '')

    let header = create('tr', id + '_header', ['table-header'], '');
    table.appendChild(header);


    // on remplit le header
    dimensions.forEach(function (e) {
        header.appendChild(
            create('td', null, null, e)
        );
    })

    t.forEach(function (e) {
        let row = create('tr', null, null, null);
        table.appendChild(row);

        for (var key in e) {
            let data;
            if (option.link_fields.includes(key)) {
                data = create('td', null, [key], null);
                let link = create('a', null, null,e[key]);
                link.href = option.link + e[key];
                data.appendChild(link);
            } else {
                data = create('td', null, [key], e[key]);
            }
            row.appendChild(data);
        };
    });

    return table;

}

// converti le résultat de set search en table html
function resultSetSearch(t) {
    let table = document.createElement('table');
    let header = document.createElement('tr');
    header.classList.add("table-header");

    let accession_col = document.createElement('td');
    accession_col.textContent = 'ACCESSION';
    header.appendChild(accession_col);

    let name_col = document.createElement('td');
    name_col.textContent = 'NOM';
    header.appendChild(name_col);

    table.appendChild(header);

    t.forEach(function (e) {
        let row = document.createElement('tr');

        let row_cell_accession = document.createElement('td');
        row_cell_accession.innerHTML = '<a href="entryInfo.php?accession=' + e.ACCESSION + '">' + e.ACCESSION + "</a>";
        row.appendChild(row_cell_accession);

        let row_cell_name = document.createElement('td');
        row_cell_name.textContent = e.PROT_NAME;
        row.appendChild(row_cell_name);

        table.appendChild(row);
    });
    return table;
}

// construis le html d'une entree
function entryToHTML(result) {
    if (result.length > 0) {

        let metadata_content = result[0];
        // creation du container
        let container = create('div', 'container', null, "");

        // affichage des meta donnees
        let metadata = create('article', 'metadata', null, '');
        container.appendChild(metadata);

        let accession = create('span', 'accession', null, metadata_content.ACCESSION);
        metadata.appendChild(accession);

        let date_creat = create('span', 'date_creat', null, "Créé le : " + metadata_content.DATECREAT);
        metadata.appendChild(date_creat);

        let date_upd = create('span', 'date_upd', null, "Modifié le : " + metadata_content.DATEUPD);
        metadata.appendChild(date_upd);

        let bdd = create('span', 'bdd', null, "BDD : " + metadata_content.DATASET);
        metadata.appendChild(bdd);

        let version = create('span', 'version', null, "Version : " + metadata_content.ENTRYVERSION);
        metadata.appendChild(version);

        let link_to_specie = create('a', 'link_to_specie', null, 'Espèce');
        metadata.appendChild(link_to_specie);
        link_to_specie.href = "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=" + metadata_content.SPECIE;

        // affichage de la séquence
        let sequence = create('article', 'sequence', null, '');
        container.appendChild(sequence)

        let seq_length = create('span', 'seq_length', null, "Longueur de séquence : " + metadata_content.SEQLENGTH);
        sequence.appendChild(seq_length);

        let seq_mass = create('span', 'seq_mass', null, "Masse de séquence : " + metadata_content.SEQMASS);
        sequence.appendChild(seq_mass);

        let seq_title = create('span','seq_title',null,'Sequence : ');
        sequence.appendChild(seq_title);

        let seq_str = create('span', 'seq_str', null, metadata_content.SEQ.replace(/(\r\n|\n|\r)/gm, "")); // https://stackoverflow.com/questions/10805125/how-to-remove-all-line-breaks-from-a-string
        sequence.appendChild(seq_str);

        // affichage des noms de prot
        let prot_names = create('article', 'prot_names', null, '');
        container.appendChild(prot_names);
        prot_names.appendChild(
            makeTable(result[1], 'prot_names_table', ['NOM DE PROTEINE', 'TYPE', 'SORTE'], null)
        );

        // affichage des noms de gene
        let gene_names = create('article', 'gene_names', null, '');
        container.appendChild(gene_names);
        gene_names.appendChild(
            makeTable(result[2], 'gene_names_table', ['NOM DE GENE', 'TYPE'], null)
        );


        // affichage des mots clés
        let keywords = create('article', 'keywords', null, '');
        container.appendChild(keywords);
        keywords.appendChild(
            makeTable(result[3], 'keywords_table', ['MOTS-CLES'], null)
        );


        // affichage des commentaires
        let comments = create('article', 'comments', null, '');
        container.appendChild(comments);
        comments.appendChild(
            makeTable(result[4], 'comments_table', ['COMMENTAIRE', 'TYPE'], null)
        );

        // affichage des dbref
        let dbref = create('article', 'dbref', null, '');
        container.appendChild(dbref);
        dbref.appendChild(
            makeTable(result[5], 'dbref_table', ['REFERENCE', 'TYPE'], {
                'link': 'https://www.ebi.ac.uk/QuickGO/term/',
                'link_fields': ['DB_REF']
            })
        );
        return container;
    } else {
        return "Aucun résultat"
    }
}