// ce script receptionne les donnees et les affice en HTML
let fSuccess;
let fError;

$(document).ready(function() {
    $("#single_search").submit(function(ev) {
        ev.preventDefault();
        ajaxSendForm(this,"GET",fSuccess,fError);
    });


    if ($('#form_accession').val() != '') {
        ajaxSendForm($('#single_search').get(0),"GET",fSuccess,fError);
    }

});

fSuccess = function(result) {
    let res = entryToHTML(result);
    $("#single_search_result").html(
        res
    )
};

fError = function(msg) {
    $("#single_search_result").html(
        msg
    );
}