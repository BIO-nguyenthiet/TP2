// script d'utilités ( repris du projet web 2018 fait en S5 )

// formulaire en json
function formToJSON(form) {
    var data = new FormData(form);
    var res = {};
    data.forEach(function (v, k) {
        res[k] = v;
    })
    return res;
}

// envoyer un requete en ajax
function ajaxSend(url, data, method, functionSuccess, functionError) {
    $.ajax({
        type: method,
        url: url,
        dataType: "json",
        data: data,
        xhrFields: {
            withCredentials: true
        },
        success: function (answer) {
            if (answer.status === "ok") {
                // le serveur a repondu
                functionSuccess(answer.result);
            } else {
                // le serveur a repondu une erreur
                functionError(answer.message);
            }
        },
        // en cas d'erreur inatendudue
        error: function (xhr, ajaxOptions, thrownError) {
            functionError(thrownError);
        }
    });
}

// envoyer le formulaire en ajax
function ajaxSendForm(form, method, functionSuccess, functionError) {
    ajaxSend(form.action, formToJSON(form), method, functionSuccess, functionError);
}

function create(tag,id,classlist,content) {
    let res = document.createElement(tag);

    if (id != null) {
        res.id = id;
    }
    if (classlist != null && classlist.length > 0) {
        classlist.forEach(function(e) {
            res.classList.add(e);
        })
    }

    res.innerHTML = content;
    return res;
}