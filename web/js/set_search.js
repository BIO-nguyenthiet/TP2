// ce script receptionne les donnees et les affice en HTML

let fSuccess;
let fError;

$(document).ready(function() {
    // quand le formulaire est envoyé, ajax récupere 
    // les donnees et JS les affiche en HTML
    $("#set_search").submit(function(ev) {
        ev.preventDefault();
        ajaxSendForm(this,"GET",fSuccess,fError);
    });
});

fSuccess = function(result) {
    // le contenu a afficher dans le div reservé aux resultats
    let res;
    if (result.length == 0) {
        res = "Aucun résultat trouvé";
    } else {
        res = resultSetSearch(result);
    }
    $("#set_search_result").html(
        res
    );
};

fError = function(msg) {
    $("#set_search_result").html(
        msg
    );
}