<?php
// Cette classe permet de communiquer avec la BD
class DataLayer
{
    // variable d'instance
    private $connexion;

    // constructeur
    public function __construct()
    {
        $this->connexion = oci_connect('c##lnguye4_a','lnguye4_a','dbinfo');
    }

    public function close() {
        oci_close($this->connexion);
    }

    // recherche un ensemble de entree selon les champs gene prot et comment
    public function set_search($gene,$prot,$comment) {
        $sql = <<<EOD
SELECT DISTINCT prot_name_2_prot.accession,p2.prot_name
    FROM protein_names p1,protein_names p2,prot_name_2_prot,gene_names,entry_2_gene_name,comments
    WHERE prot_name_2_prot.accession = entry_2_gene_name.accession
    AND entry_2_gene_name.accession = comments.accession
    AND p1.prot_name_id = prot_name_2_prot.prot_name_id
    AND gene_names.gene_name_id = entry_2_gene_name.gene_name_id
    AND UPPER(gene_names.gene_name) LIKE UPPER('%' || :gene || '%')
    AND UPPER(p1.prot_name) LIKE UPPER('%' || :prot || '%')
    AND UPPER(comments.txt_c) LIKE UPPER('%' || :comm || '%')
    AND p2.prot_name_id = prot_name_2_prot.prot_name_id
    AND p2.name_type = 'fullName' 
    AND p2.name_kind = 'recommendedName'
EOD;
        $ordre = oci_parse($this->connexion,$sql);

        oci_bind_by_name($ordre,":gene",$gene);
        oci_bind_by_name($ordre,":prot",$prot);
        oci_bind_by_name($ordre,":comm",$comment);
        oci_execute($ordre); // 66
        
        // on store le résultat
        $res = [];
        while (($row=oci_fetch_assoc($ordre))!= false) {
            $res[] = $row;
        }

        oci_free_statement($ordre);

        return $res;
    }

    public function get_from_accession($sql,$accession) {
        $ordre = oci_parse($this->connexion,$sql);

        oci_bind_by_name($ordre,":accession",$accession);
        oci_execute($ordre);
        
        $res = [];
        while (($row=oci_fetch_assoc($ordre))!= false) {
            $res[] = $row;
        }

        oci_free_statement($ordre);

        return $res;
    }


    public function get_entry($accession) {
        $sql = <<<EOD
SELECT * 
    FROM entries,proteins
    WHERE entries.accession = :accession
    AND proteins.accession = entries.accession
EOD;
        $ordre = oci_parse($this->connexion,$sql);

        oci_bind_by_name($ordre,":accession",$accession);
        oci_execute($ordre);
        
        // on store le résultat
        $res = [];
        while (($row=oci_fetch_assoc($ordre))!= false) {
            $row['SEQ'] = $row['SEQ']->read($row['SEQ']->size());
            $res[] = $row;
        }


        oci_free_statement($ordre);

        return $res;
    }

    public function get_prot_names($accession) {
        $sql = <<<EOD
SELECT protein_names.prot_name,protein_names.name_kind,protein_names.name_type
    FROM protein_names,prot_name_2_prot
    WHERE prot_name_2_prot.accession = :accession
    AND prot_name_2_prot.prot_name_id = protein_names.prot_name_id
EOD;
        return $this->get_from_accession($sql,$accession);
    }

    public function get_gene_names($accession) {
        $sql = <<<EOD
SELECT gene_names.gene_name,gene_names.name_type
    FROM gene_names,entry_2_gene_name
    WHERE entry_2_gene_name.accession = :accession
    AND entry_2_gene_name.gene_name_id = gene_names.gene_name_id
EOD;
    return $this->get_from_accession($sql,$accession);
    }

    public function get_keywords($accession) {
        $sql = <<<EOD
SELECT kw_label
    FROM entries_2_keywords,keywords
    WHERE entries_2_keywords.accession = :accession
    AND entries_2_keywords.kw_id = keywords.kw_id
EOD;
        return $this->get_from_accession($sql,$accession);
    }

    public function get_comments($accession) {
        $sql = <<<EOD
SELECT DISTINCT txt_c, type_c
    FROM comments
    WHERE accession = :accession
EOD;
        return $this->get_from_accession($sql,$accession);
    }

    public function get_dbref($accession) {
        $sql = <<<EOD
SELECT db_ref, db_type
    FROM dbref
    WHERE accession = :accession
EOD;
        return $this->get_from_accession($sql,$accession);
    }

}
