<?php

// Ce scrip est commun à tous les services. Il produit un fichier JSON et gere les eventuelles erreurs

// charge des classes eventuelles
spl_autoload_register(function ($className) {
    include ("{$className}.class.php");
});

date_default_timezone_set('Europe/Paris');
header('Content-type: application/json; charset=UTF-8');


function produceError($message)
{
    echo answer(['status' => 'error', 'message' => $message]);
}

function produceResult($result)
{
    echo answer(['status' => 'ok', 'result' => $result]);
}

function answer($reponse)
{
    echo json_encode($reponse);
}

$data = new DataLayer();
?>